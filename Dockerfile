# Start from a base image
FROM golang:1.17.2-alpine3.14

# Set the working directory
WORKDIR /app

# Copy the source code into the container
COPY . .

# Build the binary executable
RUN go mod download
RUN go build -o app .

# Expose the port that the app will run on
EXPOSE 1323

# Set the entrypoint command for the container
ENTRYPOINT [ "./app" ]
