package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"os"
)

func main() {
	var env string
	env = os.Getenv("ENV_VAR_1")
	fmt.Println(fmt.Sprintf("env -> %s", env))

	e := echo.New()
	port := ":1323"
	fmt.Println(fmt.Sprintf("http://localhost%s", port))
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World! Status Ok!")
	})
	e.Logger.Fatal(e.Start(port))
}
